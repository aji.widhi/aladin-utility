//
//  AddressValidator.swift
//  aladin-utility
//
//  Created by Aji Nugrahaning Widhi on 03/10/22.
//

import Foundation
import RxSwift

typealias AddressValidationResult = [AddressValidator.Field : AddressValidator.Result]

final class AddressValidator {
    
    static let zipChar: Int = 5
    static let rtOrRwMaxChar: Int = 3
    static let streetAddressMinChar: Int = 5
    static let streetAddressMaxChar: Int = 100
    
    enum Field {
        case province, city, district, subDistrict, zip, rt, rw, streetAddress
    }
    
    struct Result {
        let isValid: Bool
        let errorMessage: String?
    }
    
    func validate(address: Address) -> AddressValidationResult {
        [
            .province : validate(province: address.province),
            .city : validate(city: address.city),
            .district : validate(district: address.district),
            .subDistrict : validate(subDistrict: address.subDistrict),
            .zip : validate(zip: address.zip),
            .streetAddress : validate(streetAddress: address.address),
        ]
    }
    
    func validateWithRTRW(address: Address) -> AddressValidationResult {
        [
            .province : validate(province: address.province),
            .city : validate(city: address.city),
            .district : validate(district: address.district),
            .subDistrict : validate(subDistrict: address.subDistrict),
            .zip : validate(zip: address.zip),
            .rt: validateRT(rt: address.rt),
            .rw: validateRW(rw: address.rw),
            .streetAddress : validate(streetAddress: address.address),
        ]
    }
    
    func validate(province: String) -> Result {
        switch true {
        case province.isEmpty:
            return .init(isValid: false, errorMessage: nil)
        default:
            return .init( isValid: true, errorMessage: nil)
        }
    }
    
    func validate(city: String) -> Result {
        switch true {
        case city.isEmpty:
            return .init(isValid: false, errorMessage: nil)
        default:
            return .init(isValid: true, errorMessage: nil)
        }
    }
    
    func validate(district: String) -> Result {
        switch true {
        case district.isEmpty:
            return .init(isValid: false, errorMessage: nil)
        default:
            return .init(isValid: true, errorMessage: nil)
        }
    }
    
    func validate(subDistrict: String) -> Result {
        switch true {
        case subDistrict.isEmpty:
            return .init(isValid: false, errorMessage: nil)
        default:
            return .init(isValid: true, errorMessage: nil)
        }
    }
    
    func validate(zip: String) -> Result {
        switch true {
        case zip.isEmpty:
            return .init(isValid: false, errorMessage: nil)
        case zip.count != Self.zipChar:
            return .init(isValid: false, errorMessage: "Kode pos harus 5 karakter")
        default:
            return .init(isValid: true, errorMessage: nil)
        }
    }
    
    func validateRT(rt: String) -> Result {
        switch true {
        case rt.isEmpty:
            return .init(isValid: false, errorMessage: nil)
        case rt.count > Self.rtOrRwMaxChar:
            return .init(isValid: false, errorMessage: "RT tidak valid")
        default:
            return .init(isValid: true, errorMessage: nil)
        }
    }
    
    func validateRW(rw: String) -> Result {
        switch true {
        case rw.isEmpty:
            return .init(isValid: false, errorMessage: nil)
        case rw.count > Self.rtOrRwMaxChar:
            return .init(isValid: false, errorMessage: "RW tidak valid")
        default:
            return .init(isValid: true, errorMessage: nil)
        }
    }
    
    func validate(streetAddress: String) -> Result {
        switch true {
        case streetAddress.isEmpty:
            return .init(isValid: false, errorMessage: nil)
        case streetAddress.count < Self.streetAddressMinChar:
            return .init(isValid: false, errorMessage: "Alamat harus lebih dari 5 karakter")
        case streetAddress.count > Self.streetAddressMaxChar:
            return .init(isValid: false, errorMessage: "Alamat tidak boleh melebihi 100 karakter")
        default:
            return .init(isValid: true, errorMessage: nil)
        }
    }
    
}

extension AddressValidationResult {
    
    var isValid: Bool {
        self.reduce(true) { partialResult, result in
            return partialResult && result.value.isValid
        }
    }
    
}
