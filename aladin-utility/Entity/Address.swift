//
//  Address.swift
//  aladin-utility
//
//  Created by Aji Nugrahaning Widhi on 03/10/22.
//

import Foundation
import SwifterSwift

enum AddressType: Int {
    case unknown
    case ktp
    case office
    case other
}

class Address {
    
    var phoneNumber: String = .init()
    var province: String = .init()
    var city: String = .init()
    var district: String = .init()
    var subDistrict: String = .init()
    var zip: String = .init()
    var address: String = .init()
    
    var rt: String = .init()
    var rw: String = .init()
    
    var fullAddress: String {
        [address,
         rtRw.isEmpty ? nil : "RT\(rt)/RW\(rw)",
         province,
         city,
         district,
         subDistrict,
         zip
        ].compactMap { $0 }.joined(separator: ", ")
    }
    
    var rtRw: String {
        set {
            let components: [String] = newValue.components(separatedBy: "/")
            guard components.count == 2 else { return }
            rt = components[0]
            rw = components[1]
        }
        
        get {
            "\(rt)/\(rw)".trimmed
        }
    }
    
    func trimmed() -> Address {
        let trimmedAddress: Address = .init()
        trimmedAddress.phoneNumber = phoneNumber.trimmed
        trimmedAddress.province = province.trimmed
        trimmedAddress.city = city.trimmed
        trimmedAddress.district = district.trimmed
        trimmedAddress.subDistrict = subDistrict.trimmed
        trimmedAddress.zip = zip.trimmed
        trimmedAddress.address = address.trimmed
        trimmedAddress.rt = rt.trimmed
        trimmedAddress.rw = rw.trimmed
        
        return trimmedAddress
    }
    
}

class AccountAddress: Address {
    
    var addressType: AddressType = .ktp
    var isMailingAddress: Bool = false
    var provinceID: UInt64 = 0
    var cityID: UInt64 = 0
    var districtID: UInt64 = 0
    var subDistrictID: UInt64 = 0
    
    init(addressType: AddressType) {
        self.addressType = addressType
    }
    
    convenience init(ktpAddress: Address, nik: String) {
        self.init(addressType: .ktp)
        self.province = ktpAddress.province
        self.city = ktpAddress.city
        self.district = ktpAddress.district
        self.subDistrict = ktpAddress.subDistrict
        self.zip = ktpAddress.zip
        self.address = ktpAddress.address
        self.rtRw = ktpAddress.rtRw
        
        self.provinceID = UInt64(nik.prefix(2)) ?? 0
        self.cityID = UInt64(nik.prefix(4)) ?? 0
        self.districtID = UInt64(nik.prefix(6)) ?? 0
    }
    
}
